# User API
- [Get User Role](#get-user)
- [Create User Role](#create-user)
- [Edit User Role](#edit-user)
- [Delete User Role](#delete-user)

## Get User
`GET` `/api/user/:id`

**Example**
- `http://example.host/api/user/5eece9ecb4731e35f4af8350`

**Parameter**
- `:id` Valid ObjectId of user.

**Response** `200`
```json
{
  "_id": ObjectId("5eece9ecb4731e35f4af8350"),
  "line_user_id": "xxxxxx",
  "display_name": "A001",
  "email": "a001@a.com",
  "role_id": "5eece9ecb4731e35f4af8350",
  "status": "1",
  "created_at": 2020-06-02T00:00:00.000+00:00,
  "updated_at": 2020-06-14T00:00:00.000+00:00
}
```

## Create User
`POST` `/api/user`

**Example**
- `http://example.host/api/user`

**Request**
```json
{
  "line_user_id": "xxxxxx",
  "display_name": "A001",
  "email": "a001@a.com",
  "role_id": "5eece9ecb4731e35f4af8350",
  "status": "1"
}
```

**Response** `201`

## Edit User
`PUT` `/api/user/:id`

**Example**
- `http://example.host/api/user/5eece9ecb4731e35f4af8350`

**Parameter**
- `:id` Valid ObjectId of user.

**Request**
```json
{
  "display_name": "A001",
  "email": "a001@a.com",
  "role_id": "5eece9ecb4731e35f4af8350",
  "status": "1"
}
```

**Response** `200`
```json
{
  "display_name": "A001",
  "email": "a001@a.com",
  "role_id": "5eece9ecb4731e35f4af8350",
  "status": "1",
  "updated_at": 2020-06-14T00:00:00.000+00:00
}
```

## Delete User
`DELETE` `/api/user`

**Example**
- `http://example.host/api/user/5eece9ecb4731e35f4af8350`

**Parameter**
- `:id` Valid ObjectId of user.

**Response** `204`