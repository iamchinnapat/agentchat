# Agent Role API
- [Get Agent Role](#get-agentrole)
- [Create Agent Role](#create-agentrole)
- [Edit Agent Role](#edit-agentrole)
- [Delete Agent Role](#delete-agentrole)

## Get Agent Role
`GET` `/api/agentrole/:id`

**Example**
- `http://example.host/api/agentrole/5eece9ecb4731e35f4af8350`

**Parameter**
- `:id` Valid ObjectId of agent role.

**Response** `200`
```json
{
  "_id": ObjectId("5eece9ecb4731e35f4af8350"),
  "role_name": "A001",
  "role": [
    {
      "module_name" [
        {
          
        }
      ]
    }
  ]
  "status": "1",
  "created_at": 2020-06-02T00:00:00.000+00:00,
  "updated_at": 2020-06-14T00:00:00.000+00:00
}
```

## Create Agent Role
`POST` `/api/agentrole`

**Example**
- `http://example.host/api/agentrole`

**Request**
```json
{
  "role_name": "A002"
}
```

**Response** `201`

## Edit Agent Role
`PUT` `/api/agentrole/:id`

**Example**
- `http://example.host/api/agentrole/5eece9ecb4731e35f4af8350`

**Parameter**
- `:id` Valid ObjectId of agent role.

**Request**
```json
{
  "role_name": "A002",
  "status": "0"
}
```

**Response** `200`
```json
{
  "_id": ObjectId("5eece9ecb4731e35f4af8350"),
  "role_name": "A002",
  "status": "1",
  "created_at": 2020-06-02T00:00:00.000+00:00,
  "updated_at": 2020-06-14T00:00:00.000+00:00
}
```

## Delete Agent Role
`DELETE` `/api/agentrole`

**Example**
- `http://example.host/api/agentrole/5eece9ecb4731e35f4af8350`

**Parameter**
- `:id` Valid ObjectId of agent role.

**Response** `204`