# Agent Setting API
- [Get Agent Setting](#get-agentsetting)
- [Create Agent Setting](#create-agentsetting)
- [Edit Agent Setting](#edit-agentsetting)
- [Delete Agent Setting](#delete-agentsetting)

## Get Agent Setting
`GET` `/api/agentsetting/:id`

**Example**
- `http://example.host/api/agentsetting/5eece9ecb4731e35f4af8350`

**Parameter**
- `:id` Valid ObjectId of agentsetting.

**Response** `200`
```json
{
  "_id": ObjectId("5eece9ecb4731e35f4af8350"),
  "agent_code": "A001",
  "agent_name": "1",
  "agent_display_name": "ABC AAA",
  "agent_display_img": "http:/xxx.xxx/img.jpg",
  "skill": "",
  "chat_limit": 10,
  "sound_income": 1,
  "sound_request": 1,
  "ignore": 0,
  "inactive_time": 10,
  "idle_status": 1,
  "status": 1,
  "created_at": 2020-06-02T00:00:00.000+00:00,
  "updated_at": 2020-06-14T00:00:00.000+00:00
}
```
## Create Agent Setting
`POST` `/api/agentsetting`

**Example**
- `http://example.host/api/agentsetting`

**Request**
```json
{
  "agent_code": "A001",
  "agent_name": "1",
  "agent_display_name": "ABC AAA",
  "agent_display_img": "http:/xxx.xxx/img.jpg",
  "skill": "",
  "chat_limit": 10,
  "sound_income": 1,
  "sound_request": 1,
  "ignore": 0,
  "inactive_time": 10,
  "idle_status": 1,
  "status": 1
}
```

**Response** `201`

## Edit Agent Setting
`PUT` `/api/agentsetting/:id`

**Example**
- `http://example.host/api/agentsetting/5eece9ecb4731e35f4af8350`

**Parameter**
- `:id` Valid ObjectId of agentsetting.

**Request**
```json
{
  "agent_code": "A001",
  "agent_name": "1",
  "agent_display_name": "ABC AAA",
  "agent_display_img": "http:/xxx.xxx/img.jpg",
  "skill": "",
  "chat_limit": 10,
  "sound_income": 1,
  "sound_request": 1,
  "ignore": 0,
  "inactive_time": 10,
  "idle_status": 1,
  "status": 1
}
```

**Response** `200`
```json
{
  "_id": ObjectId("5eece9ecb4731e35f4af8350"),
  "agent_code": "A001",
  "agent_name": "1",
  "agent_display_name": "ABC AAA",
  "agent_display_img": "http:/xxx.xxx/img.jpg",
  "skill": "",
  "chat_limit": 10,
  "sound_income": 1,
  "sound_request": 1,
  "ignore": 0,
  "inactive_time": 10,
  "idle_status": 1,
  "status": 1
}
```

## Delete Agent Setting
`DELETE` `/api/agentsetting`

**Example**
- `http://example.host/api/agentsetting/5eece9ecb4731e35f4af8350`

**Parameter**
- `:id` Valid ObjectId of agentsetting.

**Response** `204`