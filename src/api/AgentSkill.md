# Agent Skill API
- [Get Agent Skill](#get-agentskill)
- [Create Agent Skill](#create-agentskill)
- [Edit Agent Skill](#edit-agentskill)
- [Delete Agent Skill](#delete-agentskill)

## Get Agent
`GET` `/api/agentskill/:id`

**Example**
- `http://example.host/api/agentskill/5eece9ecb4731e35f4af8350`

**Parameter**
- `:id` Valid ObjectId of agent.

**Response** `200`
```json
{
  "_id": ObjectId("5eece9ecb4731e35f4af8350"),
  "skill_name": "A001",
  "status": "1",
  "created_at": 2020-06-02T00:00:00.000+00:00,
  "updated_at": 2020-06-14T00:00:00.000+00:00
}
```

## Create Agent
`POST` `/api/agentskill`

**Example**
- `http://example.host/api/agentskill`

**Request**
```json
{
  "skill_name": "A002"
}
```

**Response** `201`

## Edit Agent
`PUT` `/api/agentskill/:id`

**Example**
- `http://example.host/api/agentskill/5eece9ecb4731e35f4af8350`

**Parameter**
- `:id` Valid ObjectId of agent.

**Request**
```json
{
  "skill_name": "A002",
  "status": "0"
}
```

**Response** `200`
```json
{
  "_id": ObjectId("5eece9ecb4731e35f4af8350"),
  "skill_name": "A002",
  "status": "1",
  "created_at": 2020-06-02T00:00:00.000+00:00,
  "updated_at": 2020-06-14T00:00:00.000+00:00
}
```

## Delete Agent
`DELETE` `/api/agentskill`

**Example**
- `http://example.host/api/agent/5eece9ecb4731e35f4af8350`

**Parameter**
- `:id` Valid ObjectId of agent.

**Response** `204`