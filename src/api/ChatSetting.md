# Chat Setting API
- [Get Chat Setting](#get-chatsetting)
- [Create Chat Setting](#create-chatsetting)
- [Edit Chat Setting](#edit-chatsetting)
- [Delete Chat Setting](#delete-chatsetting)

## Get Chat Setting
`GET` `/api/chatsetting/:id`

**Example**
- `http://example.host/api/chatsetting/5eece9ecb4731e35f4af8350`

**Parameter**
- `:id` Valid ObjectId of Chat Setting.

**Response** `200`
```json
{
  "_id": ObjectId("5eece9ecb4731e35f4af8350"),
  "chatsetting_name": "A001",
  "respond_time": [
      
  ],
  "respond_autokick": 0,
  "respond_autokick_min": 10,
  "respond_autokick_msg": "",
  "userenter_msg": [
      {
        "keyword": [],
        "message": []
      }
    ],
  "welcome_msg": "",
  "queing_msg": "",
  "outoffice_msg": [
      {
        "message": "",
        "afer5min": ""
      }
    ],
  "status": "1",
  "created_at": 2020-06-02T00:00:00.000+00:00,
  "updated_at": 2020-06-14T00:00:00.000+00:00
}
```

## Create Chat Setting
`POST` `/api/chatsetting`

**Example**
- `http://example.host/api/chatsetting`

**Request**
```json
{
  "chatsetting_name": "A001",
  "respond_autokick": 0,
  "respond_autokick_min": 10,
  "respond_autokick_msg": "",
  "userenter_msg": [
      {
        "keyword": [],
        "message": []
      }
    ],
  "welcome_msg": "",
  "queing_msg": "",
  "outoffice_msg": [
      {
        "message": "",
        "afer5min": ""
      }
    ],
  "status": "1"
}
```

**Response** `201`

## Edit Chat Setting
`PUT` `/api/chatsetting/:id`

**Example**
- `http://example.host/api/chatsetting/5eece9ecb4731e35f4af8350`

**Parameter**
- `:id` Valid ObjectId of Chat Setting.

**Request**
```json
{
  "chatsetting_name": "A001",
  "respond_autokick": 0,
  "respond_autokick_min": 10,
  "respond_autokick_msg": "",
  "userenter_msg": [
      {
        "keyword": [],
        "message": []
      }
    ],
  "welcome_msg": "",
  "queing_msg": "",
  "outoffice_msg": [
      {
        "message": "",
        "afer5min": ""
      }
    ],
  "status": "1"
}
```

**Response** `200`
```json
{
  "_id": ObjectId("5eece9ecb4731e35f4af8350"),
  "chatsetting_name": "A001",
  "respond_autokick": 0,
  "respond_autokick_min": 10,
  "respond_autokick_msg": "",
  "userenter_msg": [
      {
        "keyword": [],
        "message": []
      }
    ],
  "welcome_msg": "",
  "queing_msg": "",
  "outoffice_msg": [
      {
        "message": "",
        "afer5min": ""
      }
    ],
  "status": "1",
  "updated_at": 2020-06-14T00:00:00.000+00:00
}
```

## Delete Chat Setting
`DELETE` `/api/chatsetting`

**Example**
- `http://example.host/api/chatsetting/5eece9ecb4731e35f4af8350`

**Parameter**
- `:id` Valid ObjectId of Chat Setting.

**Response** `204`