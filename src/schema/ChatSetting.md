# Chat Setting Schema

| Field               | Type      | Index  | Unique | *          |
| ---                 | ---       | :---:  | :---:  | ---        |
| _id                 | ObjectId  | Y      | Y      |            |
