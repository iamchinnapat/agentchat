# Agent Setting Schema

| Field               | Type      | Index  | Unique | *              |
| ---                 | ---       | :---:  | :---:  | ---            |
| _id                 | ObjectId  | Y      | Y      |                |
| agent_code          | String    | Y      | Y      |                |
| agent_name          | String    | Y      |        |                |
| agent_display_name  | String    | Y      |        |                |
| agent_display_img   | String    |        |        |                |
| skill               | []        |        |        |                |
| chat_limit          | String    |        |        |                |
| sound_income        | String    |        |        |                |
| sound_request       | String    |        |        |                |
| ignore              | Integer   |        |        | Min(`0`)       |
| inactive_time       | Integer   |        |        | Min(`0`)       |
| idle_status         | Integer   |        |        | Min(`0`)       |
| status              | Integer   |        |        | Min(`0`)       |
| created_at          | Timestamp |        |        | Auto manage    |
| updated_at          | Timestamp |        |        | Auto manage    |