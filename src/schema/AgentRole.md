# Agent Role Schema

| Field               | Type      | Index  | Unique | *              |
| ---                 | ---       | :---:  | :---:  | ---            |
| _id                 | ObjectId  | Y      | Y      |                |
| role_name           | String    | Y      |        |                |
| status              | Integer   | N      |        | Min(`0`)       |
| created_at          | Timestamp |        |        | Auto manage    |
| updated_at          | Timestamp |        |        | Auto manage    |