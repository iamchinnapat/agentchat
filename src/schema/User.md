# User Schema

| Field           | Type      | Index  | Unique | *                |
| ---             | ---       | :---:  | :---:  | ---              |
| _id             | ObjectId  | Y      | Y      |                  |
| line_user_id    | String    | Y      | Y      |                  |
| display_name    | String    |        |        |                  |
| email           | String    |        | Y      |                  |
| role_id         | Ref       | Y      |        |                  |
| created_at      | Timestamp |        |        | Auto manage      |
| updated_at      | Timestamp |        |        | Auto manage      |
