# AgentChat

# Table of contents
- API
  - [User](/src/api/User.md)
  - [AgentRole](/src/api/AgentRole.md)
  - [AgentSkill](/src/api/AgentSkill.md)
  - [AgentSetting](/src/api/AgentSetting.md)
  - [ChatSetting](/src/api/ChatSetting.md)
- Schema
  - [User](/src/schema/User.md)
  - [AgentRole](/src/schema/AgentRole.md)
  - [AgentSkill](/src/schema/AgentSkill.md)
  - [AgentSetting](/src/schema/AgentSetting.md)
  - [ChatSetting](/src/schema/ChatSetting.md)